<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['page_id', 'path'];

    public function page() {
        return $this->belongsTo('App\Page');
    }
    
}
