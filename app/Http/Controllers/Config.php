<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException;



 class Config {

	static function get($name) {
		
		$COMMERCE_CONFIG = array(
			"APIKEY" => "5758EF38-4070-4908-A21D-9E694D218L40", // Registre aquí su apiKey
			"SECRETKEY" => "ffd4e1965db37cd6468f889e8757f7418c63b0f6", // Registre aquí su secretKey
			"APIURL" => "https://www.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
			//"BASEURL" => "http://167.99.118.90/apiFlow" //Registre aquí la URL base en su página 
			"BASEURL" => "http://167.99.118.90/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
		 );

		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new ModelNotFoundException("The configuration element thas not exist", 1);
		}
		return $COMMERCE_CONFIG[$name];
	}
 }
