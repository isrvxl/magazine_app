<?php

namespace App\Http\Controllers;

use App\{InfoPage, Sale, Magazine, Subscription};
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\FlowApi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->type == 1) {
            $magazines = Magazine::where('status', 1)->get();
            $infopage = InfoPage::where('id', 1)->get();
            return view('home', ['magazines' => $magazines,'infopage' => $infopage]);
        }else if ($user->type == 2) {

            $sub = Subscription::find($user->subscription_id);
            
            if(count($user->sales) > 0 ){
                return redirect('/userSuccess')->with('status', 'Contraseña Cambiada');
            }
            $total = $sub->price;
            if($total == 0) {
                if(count($user->sales) == 0) {
                    $sale = new Sale;
                    $sale->user_id = $user->id;
                    $sale->subscription_id = $user->subscription_id;
                    $sale->amount = $total;
                    $sale->status = 1;
                    $sale->save();

                    return redirect('/userSuccess')->with('status', 'Suscripción Aprobada!');
            
                }else {
                    return redirect('/userSuccess')->with('status', 'Tu Suscripción Gratuita ah pasado!');
                }
            }

            
            $sale = new Sale;
            $sale->user_id = $user->id;
            $sale->subscription_id = $user->subscription_id;
            $sale->amount = $total;
            $sale->status = 9;
            $sale->save();
            
            $optional = array(
                "id" => $sale->id,
            );
            $optional = json_encode($optional);

            $params = array(
                "commerceOrder" => 'FuegoPentecostes S000-'.$sale->id,
                "subject" => "Pago en Pentecostes",
                "currency" => "CLP",
                "amount" => $total,
                "email" => $user->email,
                "paymentMethod" => 9,
                "urlConfirmation" =>  "http://167.99.118.90/flowConfirm",
                "urlReturn" => "http://167.99.118.90/flowResult",
                "optional" => $optional
            );

            $serviceName = "payment/create";
            $flowApi = new FlowApi();
            $response = $flowApi->send($serviceName, $params,"POST");
            $redirect = $response["url"] . "?token=" . $response["token"];

            return redirect($redirect);
        }

        
    }

    
}
