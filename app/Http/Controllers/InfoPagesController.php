<?php

namespace App\Http\Controllers;

use App\InfoPage;
use Illuminate\Http\Request;
use App\Http\Requests\StoreInfoPagePost;

class InfoPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('infoPages.create', ['infopage'=> new InfoPage()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInfoPagePost $request)
    {
        $infopage = InfoPage::create($request->all());

        return view('infoPages.edit', ['infopage' => $infopage])->with('status', 'Creado Con Exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(InfoPage $infopage)
    {
        return view('infoPages.edit', ['infopage' => $infopage]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InfoPage $infopage)
    {
        $infopage->update($request->all());

        return view('infoPages.edit', ['infopage' => $infopage])->with('status', 'Acutalizado Con Exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
