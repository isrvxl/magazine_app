<?php

namespace App\Http\Controllers;

use URL;
use App\Page;
use App\Magazine;
use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreMagazinePost;

class MagazineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::get();
        $user = Auth::user();     
        $magazines = Magazine::orderBy('edit_date','desc')->get();

        return view('magazines.list', ['magazines' => $magazines, 'user' => $user, 'pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectStatus = Magazine::STATUS;
        $user = Auth::user();
        return view('magazines.create', ['magazine'=> new Magazine(), 'user' => $user, 'selectStatus' => $selectStatus]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMagazinePost $request)
    {
        $magazine = Magazine::create($request->all());

        if ($request->file('img')) {
			$path = $request->file('img')->store('public');
			$img  = str_replace('public/', '', $path);
			$magazine->img = $img;
			$magazine->save();
        }
        
        if ($request->file('background_image')) {
            $path = $request->file('background_image')->store('public');
            $img  = str_replace('public/', '', $path);
            $magazine->background_image = $img;
            $magazine->save();
        }

        return back()->with('status', 'Revista Creada');
			
    }      

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Magazine $magazine)
    {
        $selectStatus = Magazine::STATUS;
        $user = Auth::user();
        return view('magazines.edit', ['magazine' => $magazine, 'user' => $user, 'selectStatus' => $selectStatus]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Magazine $magazine)
    {
        $magazine->update($request->all());

        if ($request->file('img')) {
			$path = $request->file('img')->store('public');
			$img  = str_replace('public/', '', $path);
			$magazine->img = $img;
			$magazine->save();
        }
        
        if ($request->file('background_image')) {
            $path = $request->file('background_image')->store('public');
            $img  = str_replace('public/', '', $path);
            $magazine->background_image = $img;
            $magazine->save();
        }

        return back()->with('status', 'Revista Actualizada');
			
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Magazine $magazine)
    {
        $magazine->delete();

        return back()->with('status', 'Revista Eliminada');
    }

    public function contentImage(Request $request) {

        $filename = time() . "." . $request->fileToUpload->extension();

        $request->fileToUpload->move(public_path('images_ckeditor'), $filename);

        return response()->json(['success'=>true,'file'=> URL::to('/') . '/images_ckeditor'.'/'. $filename]);
    }

    public function contentGallery(Request $request, $id) {
        
        $filename = time() . "." . $request->file->extension();

        $request->file->move(public_path('images_gallery'), $filename);
        
        $gallery = new Gallery([
            'path' => $filename,
            'page_id'=> $id
        ]);

        $gallery->save();

        return response()->json(['success'=>true,'file'=> URL::to('/') . '/images_gallery'.'/'. $filename]);
    }

    public function pages($id)
    {     
        $magazine = Magazine::findOrFail($id);
        $pages = Page::where('magazine_id',$magazine->id)->orderBy('id','asc')->get();
        $selectTypes = Page::TYPES;  
        return view('mPages.list', ['magazine' => $magazine,'pages' => $pages, 'selectTypes' => $selectTypes]);
    }

}

