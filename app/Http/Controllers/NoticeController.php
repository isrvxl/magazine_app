<?php

namespace App\Http\Controllers;

use App\{Notice, User};
use Illuminate\Http\Request;
use Auth;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::orderBy('for_at', 'desc')->get();
        return view('notices.list', ['notices' => $notices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    function sendPushNotification($fields = array()){
        $API_ACCESS_KEY = 'AAAA4AXPCkI:APA91bHRdHoxGPJafhGwu7JHIc81muQPdgBkV58UQEoVP4foswgvXiGc1TTGJZPef4VOWQu89pH2wVjfYXBDRh7QJY3u9OdVFLFO3o-N8VsNbcl72ssgO_2qBDEzojgqR4HCOMQL23zP';
        $headers = array
        (
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notice = Notice::create([
            'title'     => $request->title,
            'body'      => $request->body,
            'user_id'   => Auth::user()->id,
            'type'      => $request->type,
            'for_at'      => $request->for_at,
        ]);

        if($request->type == 1) {
            $users = User::where('type','<>', 1)->get();
            foreach ($users as $key => $u) {
                if($u->fcm_token) {
                    $fields = array(
                        "to" => $u->fcm_token,
                        "notification" => array(
                            "title" => $notice->title,
                            "body" => $notice->body,
                        ),
                        "data" => array(
                            "click_action" => "FLUTTER_NOTIFICATION_CLICK"
                        )
                    );
                    $this->sendPushNotification($fields);
                } 
            }
        }

        return back()->with('status', 'Aviso Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show(Notice $notice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        Notice::find($id)->delete();
        return back()->with('status', 'Aviso Eliminado');
    }
}
