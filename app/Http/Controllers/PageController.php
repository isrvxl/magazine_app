<?php

namespace App\Http\Controllers;

use App\Page;
use App\Gallery;
use App\Magazine;
use Illuminate\Http\Request;
use App\Http\Requests\StorePagePost;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $selectTypes = Page::TYPES;   
        $pages = Page::orderBy('page','asc')->get();
        $magazines = Magazine::get();
        return view('mPages.list', ['pages' => $pages, 'magazines' => $magazines, 'selectTypes' => $selectTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectTypes = Page::TYPES; 
        $magazines = Magazine::get();
        return view('mPages.create', ['page'=> new Page(), 'magazines' => $magazines, 'selectTypes' => $selectTypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePagePost $request)
    {
                
        $pages = Page::create($request->all());
        $magazines = Magazine::get();

        if ($request->file('img')) {
			$path = $request->file('img')->store('public');
			$img  = str_replace('public/', '', $path);
			$page->img = $img;
			$page->save();
        }
        
        return back()->with('status', 'Pagina Creada');

    }

    public function pagePost(Request $request) {
        
        $page = Page::create($request->all());
        
        if ($request->file('img')) {
			$path = $request->file('img')->store('public');
			$img  = str_replace('public/', '', $path);
			$page->img = $img;
			$page->save();
        }
        
        return back()->with('status', 'Pagina Creada');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Page::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $selectTypes = Page::TYPES; 
        $page = Page::find($id);
        return view('mPages.edit', ['page' => $page, 'selectTypes' => $selectTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $page->update($request->all());
        if ($request->file('img')) {
			$path = $request->file('img')->store('public');
			$img  = str_replace('public/', '', $path);
			$page->img = $img;
			$page->save();
        }
        
        return back()->with('status', 'Pagina Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);
        $magazineId = $page->magazine->id;
        $page->delete();
        return redirect('/magazinePage/'.$magazineId)->with('status', 'Pagina Eliminada');
    }

    public function galleryDelete($id) {
        Gallery::find($id)->delete();
        return back()->with('status', 'Imagen Galeria Borrada');
    }

}

