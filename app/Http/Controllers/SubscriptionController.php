<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSubscriptionPost;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subs = Subscription::get();

        return view('subscriptions.list', ['subs' => $subs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subscriptions.create', ['sub'=> new Subscription()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubscriptionPost $request)
    {
        Subscription::create($request->all());

        return back()->with('status', 'Suscripcion Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        return view('subscriptions.edit', ['subscription' => $subscription]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSubscriptionPost $request, Subscription $subscription)
    {
        $subscription->update($request->validated()); 

        return back()->with('status', 'Suscripcion Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        $subscription->delete();

        return back()->with('status', 'Suscripcion Eliminada');
    }

    public function changeSub($id)
    {
        $user = Auth::user();
        $user->subscription_id = $id;
        if($user->update()){
            return 1;
        }

    }

    public function subscriptionselect()
    {
        $subs = Subscription::all();
        $data = "";
        foreach ($subs as $v) {
            $data .= sprintf(
                '<option value="%s">%s %s / %s</option>',
                $v->id,
                $v->name,
                $v->description,
                $v->price
            );
        }
        return $data;
    }
}
