<?php

namespace App\Http\Controllers;

use App\User;
use App\{Subscription, Magazine, Page, Sale};
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserPost;
use App\Http\Requests\UpdateUserPut;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\{Payer, Payment, Amount, RedirectUrls, Transaction };
use Auth;

class UserController extends Controller
{
    public function appRegister() {
        $subs = Subscription::all();
        return view('auth.register', ['subs' => $subs]);
    }

    public function resetPass() {
        return view('resetPass');
    }

    public function landing() {
        $magazines = Magazine::where('status',1)->get();
       
        foreach ($magazines as $key => $m) {
            
            $pages = Page::where('magazine_id', $m->id)->orderBy('page')->get();

            $html = "<ul>";
            foreach ($pages as $key => $p) {
                $html .= "<li>".$p->page.".- ".$p->title."</li>";
            }
            $html .= "</ul>";

            $m->indexedpages = $html;
            
        }
        return view('welcome', ['magazines' => $magazines]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selectTypes = User::TYPES;
        $users = User::orderBy('id','asc')->get();
        $subs = Subscription::get();
        return view('users.list', ['users' => $users, 'subs' => $subs, 'selectTypes' => $selectTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectTypes = User::TYPES;
        $subs = Subscription::get();
        return view('users.create', ['user'=> new User(), 'selectTypes' => $selectTypes, 'subs' => $subs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserPost $request)
    {
        $subs = Subscription::get();
        
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'type' => $request['type'],
            'subscription_id' => $request['subscription_id'],
        ]);

        return back()->with('status', 'Usuario Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $selectTypes = User::TYPES;
        $subs = Subscription::get();
        return view('users.edit', ['user' => $user, 'selectTypes' => $selectTypes, 'subs' => $subs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserPut $request, User $user)
    {
        $user->update(
            [
            'name' => $request['name'],
            'email' => $request['email'],
            'type' => $request['type'],
            'subscription_id' => $request['subscription_id'],
            ]
        );

        return back()->with('status', 'Usuario Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return back()->with('status', 'Usuario Eliminado');
    }

    public function userSuccess() {
        return view('userSuccess');
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

    public function registerApp(Request $request) {
        
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'subscription_id' => 'required|integer',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'subscription_id' => $request->subscription_id,
            'type' => 2,
        ]);

        $sub = Subscription::find($user->subscription_id);
        $total = $sub->price;
        if($total == 0) {
            if(count($user->sales) == 0) {
                $sale = new Sale;
                $sale->user_id = $user->id;
                $sale->subscription_id = $user->subscription_id;
                $sale->amount = $total;
                $sale->status = 1;
                $sale->save();

                return redirect('/userSuccess')->with('status', 'Suscripción Aprobada!');
        
            }else {
                return redirect('/userSuccess')->with('status', 'Tu Suscripción Gratuita ah pasado!');
            }
        }

        
        $sale = new Sale;
        $sale->user_id = $user->id;
        $sale->subscription_id = $user->subscription_id;
        $sale->amount = $total;
        $sale->status = 9;
        $sale->save();
        
        if($request->type == 1) {
            $optional = array(
                "id" => $sale->id,
            );
            $optional = json_encode($optional);

            $params = array(
                "commerceOrder" => 'FuegoPentecostes S000-'.$sale->id,
                "subject" => "Pago en Pentecostes",
                "currency" => "CLP",
                "amount" => $total,
                "email" => $user->email,
                "paymentMethod" => 9,
                "urlConfirmation" =>  "http://167.99.118.90/flowConfirm",
                "urlReturn" => "http://167.99.118.90/flowResult",
                "optional" => $optional
            );

            $serviceName = "payment/create";
            $flowApi = new FlowApi();
            $response = $flowApi->send($serviceName, $params,"POST");
            $redirect = $response["url"] . "?token=" . $response["token"];

            return redirect($redirect);
        }else if( $request->type == 2) {

            $paypalConfig = Config::get('paypal');
            
            $apiContext = new ApiContext(
                new OAuthTokenCredential(
                    $paypalConfig['client_id'],
                    $paypalConfig['secret']
                )
            );

            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $amount = new Amount();
            $amount->setTotal($sub->dollar_price);
            $amount->setCurrency('USD');

            $transaction = new Transaction();
            $transaction->setAmount($amount);
            $transaction->setDescription($sub->name);
            $transaction->setInvoiceNumber($sale->id);

            $returnUrl = url("/paypalReturn");
            $paypalUrl = url("/paypalCancel");

            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl($returnUrl)
                ->setCancelUrl($paypalUrl);

            $payment = new Payment();
            $payment->setIntent('sale')
                ->setPayer($payer)
                ->setTransactions(array($transaction))
                ->setRedirectUrls($redirectUrls);

            try {
                $payment->create($apiContext);
                return redirect()->away($payment->getApprovalLink());
            }
            catch (\PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getData();
            }


        }

        
    }
}
