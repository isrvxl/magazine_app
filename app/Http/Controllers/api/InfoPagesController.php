<?php

namespace App\Http\Controllers\api;

use App\InfoPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfoPagesController extends ApiResponseController
{
    public function index()
    {
        $infopage = InfoPage::get();
        return $this->successResponse($infopage);
    }

    public function show(InfoPage $infopage)
    {   
        return $this->successResponse($infopage);
    }
}
