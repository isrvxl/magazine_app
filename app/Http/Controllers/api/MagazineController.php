<?php

namespace App\Http\Controllers\api;

use App\{Magazine, Page};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;

class MagazineController extends ApiResponseController
{
    public function orderCall($object1, $object2) { 
        return $object1['page'] > $object2['page']; 
    } 

    public function index()
    {
        $user = Auth::user();
        
        if ($user->type === 2) {
            $cDate = new Carbon($user->created_at);
            $sale = $user->sales->last();
            $months = $sale->subscription->months;
            $dDate = new Carbon($user->created_at);
            $to = $dDate->addMonths($months);
            $magazines = Magazine::where('edit_date', '>=', $cDate->firstOfMonth())->where('edit_date','<=',$to )->where('status',1)->get();
        } else {
            $magazines = Magazine::where('status',1)->get();
        }
        
       
        foreach ($magazines as $key => $m) {
            
            $pages = Page::where('magazine_id', $m->id)->orderBy('page')->get();

            $html = "<ul>";
            foreach ($pages as $key => $p) {
                $html .= "<li>".$p->page.".- ".$p->title."</li>";
            }
            $html .= "</ul>";

            $m->indexedpages = $html;
            
        }
        
        return $this->successResponse($magazines);
    }

    public function show(Magazine $magazine)
    {   
        return $this->successResponse($magazine);
    }

    public function magazinePages($id) {
        $pages = Page::with('galleries')->where('magazine_id', $id)->orderBy('page')->get();
        foreach ($pages as $key => $p) {
            foreach ($p->galleries as $k => $g) {
                $p->galleries[$k] = $g->path;
            }
        }
        return $this->successResponse($pages);
    }

}

