<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notice;
use Carbon\Carbon;

class NoticeController extends ApiResponseController
{
    public function index()
    {
        $from = new Carbon('first day of this month');
        $to = new Carbon('last day of this month');
        $notices = Notice::whereBetween('for_at', [$from->format('Y-m-d'), $to->format('Y-m-d')])->orderBy('for_at')->where('type', 2)->get();

        foreach ($notices as $key => $n) {
            $notices[$key]['dd'] = date("d-m-Y", strtotime($n->for_at));
        }
        
        return $this->successResponse($notices);
    }

}
