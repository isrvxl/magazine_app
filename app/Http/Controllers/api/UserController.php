<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends ApiResponseController
{

    public function index()
    {
        $users = User::
        join('subscriptions', 'subscriptions.id', '=', 'users.subscription_id')->
        select('users.*', 'subscriptions.name as subscription')->get();
        return $this->successResponse($users);
    }

    public function show(User $user)
    {   
        $user->subscription;
        return $this->successResponse($user);
    }

    public function subscription(Subscription $subscription)
    {   
        return $this->successResponse(["users" => $subscription->user, "subscription" => $subscription]);
    }


}
