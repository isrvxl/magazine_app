<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMagazinePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'img' => 'required|mimes:jpeg,bmp,png',
            'background_image' => 'required|mimes:jpeg,bmp,png',
            'description' => 'required',
            'edit_date' => 'required',
            'created_by' => 'required',
            'status' => 'required|integer',
        ];
    }
}
