<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePagePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'magazine_id' => 'required',
            'content' => 'required',
            'page' => 'required',
            'type' => 'required',
            'img' => 'required|mimes:jpeg,bmp,png',
            'video_url' => 'required',
        ];
    }
}
