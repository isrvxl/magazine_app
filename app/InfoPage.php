<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoPage extends Model
{
    protected $fillable = [
        'title', 'description', 'type'
    ];
}
