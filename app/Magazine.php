<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    protected $fillable = [
        'name', 'img', 'description', 'edit_date', 'created_by','status', 'background_image'
    ];

    public function pages()
    {
        return $this->hasMany('App\Page');
    }
    const STATUS = [
        1 => 'Publicada',
        2 => 'No Publicada'
    ];
    public function getStatuslabelAttribute(){
        return Self::STATUS[$this->status];
    }

    public function user() {
        return $this->belongsTo('\App\User','created_by');
    }

    public function indexpages() {
        
        function order($object1, $object2) { 
            return $object1->page > $object2->page; 
        } 

        $pages = $this->hasMany('App\Page');
        usort($pages->toArray(), 'order');
        $html = "<ul>";
        foreach ($pages as $key => $p) {
            $html .= "<li>".$p->page.".- ".$p->title."</li>";
        }
        $html .= "</ul>";

        return $html;
    }
}
