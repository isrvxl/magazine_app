<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $fillable = ['title', 'body', 'user_id', 'type', 'for_at'];

    public function user() {
        return $this->belongsTo('\App\User');
    }

    const TYPES = [
        1 => 'Push',
        2 => 'Aviso',
    ];

    public function getTypelabelAttribute(){
        return Self::TYPES[$this->type];
    }
}
