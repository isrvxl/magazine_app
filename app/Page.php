<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title', 'magazine_id', 'content', 'page', 'type', 'img', 'video_url'
    ];

    const TYPES = [
        1 => 'Indice',
        2 => 'Pagina',
    ];

    public function magazine() {
        return $this->belongsTo('\App\Magazine');
    }


    public function galleries() {
        return $this->hasMany('\App\Gallery');
    }

    public function getTypelabelAttribute(){
        return Self::TYPES[$this->type];
    }
}
