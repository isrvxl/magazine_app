<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'user_id', 'subscription_id', 'amount', 'status'
    ];

    const STATUS = [
        9=>"Pendiente",
        1=>'Aprobada',
        2=>'Anulada' 
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
    
    public function getStatusLabelAttribute()
    {
        return Self::STATUS[$this->status];
    }
}
