<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'name', 'price', 'months', 'dollar_price'
    ];

    public function user(){
        return $this->hasMany(User::class);
    }
}