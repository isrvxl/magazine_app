<?php

namespace App;

use App\Subscription;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyResetPassword;


class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'subscription_id', 'fcm_token'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function subscription(){
        return $this->belongsTo(Subscription::class);
    }

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }

    const TYPES = [
        1 => 'Admin',
        2 => 'Cliente',
        3 => 'Free Pass'
    ];

    const STATUS = array(
        1 => 'Pendiente',
        2 => 'Activo',
        3 => 'Cancelado',
        4 => 'Renovacion'
    );

    public function getTypelabelAttribute(){
        return Self::TYPES[$this->type];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }
}
