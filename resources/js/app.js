const ClassicEditor = require('@ckeditor/ckeditor5-build-classic')

import MyUploadAdapter from "./assets/ckeditor/MyUploadAdapter"

function MyCustomUploadAdapterPlugin( editor ) {
    editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
        // Configure the URL to the upload script in your back-end here!
        return new MyUploadAdapter( loader );
    };
}

ClassicEditor
    .create( document.querySelector( '#description' ), {
        extraPlugins: [ MyCustomUploadAdapterPlugin ],
       

        // ...
    } )
    .catch( error => {
        console.log( error );
    } );
