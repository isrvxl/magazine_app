@extends('layouts.auth')

@section('content')
<div class="body">
           

             
    <form  id="sign_in" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="msg">Ingresa para entrar al sistema</div>
        
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">person</i>
            </span>
            <div class="form-line">
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        
        </div>
        <div class="row">
            <div class="col-xs-8 p-t-5">
                <input type="checkbox" name="remember" id="remember" class="filled-in chk-col-pink">
                <label for="rememberme">Recuerdame</label>
            </div>
            <div class="col-xs-4">
                <button class="btn btn-block bg-pink waves-effect" type="submit">Ingresar</button>
            </div>
        </div>
        <div class="form-group row mb-0 text-center">
            <a href="/password/reset">Olvidaste tu Contraseña?</a>
        </div>
    </form>
</div>
@endsection
