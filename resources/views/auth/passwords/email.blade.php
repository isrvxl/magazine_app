@extends('layouts.auth')

@section('content')
<div class="body">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    

    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="msg">Restablece tu Contraseña</div>

        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">person</i>
            </span>
            <div class="form-line">
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="">
                <button class="btn btn-block bg-pink waves-effect" type="submit">Mandar Link para cambiar Contraseña?</button>
            </div>
        </div>
    </form>
</div>
@endsection
