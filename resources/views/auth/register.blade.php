@extends('layouts.auth')

@section('content')
<div class="body">
    <form method="POST" action="/registerApp">
        @csrf
        <input type="hidden" id="typeInput" name="type" value="">
        <div class="msg">Registrate en nuestro sistema</div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">person</i>
            </span>
            <div class="form-line">
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Nombre y Apellido" required autofocus>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">alternate_email</i>
            </span>
            <div class="form-line">
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" placeholder="Confirmar Password" required>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">attach_money</i>
            </span>
            <div class="form-line">
                <select class="form-control @error('subscription_id') is-invalid @enderror" name="subscription_id" id="" required>
                    <option value="">Seleccione</option>
                    @foreach ($subs as $s)
                        <option value="{{$s->id}}">{{$s->price}} - {{$s->name}} </option>
                    @endforeach
                </select>
                @error('subscription_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        
        </div>

     

        <div class="form-group row mb-0 text-center">
            
            <button type="button" data-type="1" class="registerButton btn btn-primary" >
                {{ __('Registro con Flow') }}
            </button>
            <button type="button" data-type="2" class="registerButton btn btn-primary" >
                {{ __('Registro con Paypal') }}
            </button>
            
        </div>
        <div class="form-group row mb-0 text-center">
            
            <a href="/password/reset">Olvidaste tu Contraseña?</a>
            
        </div>
    </form>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('.registerButton').click(function(){
                var type = $(this).data('type');
                $('#typeInput').val(type)
                $('form').submit()
            })
        })
    </script>
@endsection
