@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>
            Catalogo
        </h2>
    </div>
    <div class="row clearfix">
        <!-- Basic Example -->

        @foreach ($magazines as $m)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <div class="card">
                    <div class="header">
                        <h2>{{$m->name}}</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{ route('magazine.edit', $m->id) }}">Editar</a></li>
                                    <li><a href="/magazinePage/{{$m->id}}">Paginas</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                <img src="/storage/{{$m->img}}" />
                                </div>
                                <div class="item">
                                    <img src="/storage/{{$m->background_image}}" />
                                </div>
                                
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
      
    </div>
</div>
@endsection
