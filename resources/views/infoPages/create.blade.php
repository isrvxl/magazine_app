@extends('layouts.app')

@section('css')
     <link rel="stylesheet" href="/trumbowyg/dist/ui/trumbowyg.min.css">
     <link rel="stylesheet" href="/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.min.css">
@endsection

@section('content')

@if ($errors->any())
@foreach ($errors->all() as $error)
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{$error}}
    </div> 
@endforeach

@endif

    <form action="{{ route('infopages.store')}}" method="POST" enctype="multipart/form-data">

        @csrf
        <input type="hidden" name="type" value=1>
        <input type="hidden" id="token" value="{{csrf_token()}}">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        
        <div class="row clearfix">
            <div class=" col-md-offset-1 col-lg-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Acerca de Nosotros
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row ">
                            <div class="col-md-12">
                                <label for="">Titulo</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">title</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" name="title" id="title" class="form-control" placeholder="Titulo">
                                    </div>
                                </div>
                            </div>  
                            <div class="col-md-12">
                                <label for="">Descripcion</label>
                                <textarea name="description" id="trumbowyg-demo" class="form-control"></textarea>
                            </div>
                            <div class=" row col-md-12 text-right">
                                <input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
                            </div>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="/trumbowyg/dist/trumbowyg.min.js"></script>
    <script src="/trumbowyg/dist/plugins/lineheight/trumbowyg.lineheight.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
    <script src="/trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js"></script>
    <script src="/trumbowyg/dist/plugins/insertaudio/trumbowyg.insertaudio.min.js"></script>
    <script src="/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js"></script>
    <script type="/text/javascript" src="/trumbowyg/dist/langs/es.min.js"></script>
        <script>
            $(function(){
                $('#trumbowyg-demo').trumbowyg({
                    lang: 'es',
                    btns: [
                        
                        ['fontfamily','formatting','strong', 'em', 'del'],
                        ['foreColor', 'backColor'],
                        ['link'],
                        ['insertImage','upload','insertAudio'],
                        ['unorderedList', 'orderedList'],
                        ['viewHTML'],
                        ['undo', 'redo'], // Only supported in Blink browsers
                        
                        ['horizontalRule'],
                        ['lineheight','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                        ['fullscreen'],
                    ],
                    plugins: {
                        upload: {
                            serverPath:"http://fuegodepentecostes.cl/contentImage"
                        }
                    }
                });
            })
        </script>
@endsection