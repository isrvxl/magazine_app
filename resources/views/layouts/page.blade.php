<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Fuego de Pentecostes-App</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/themes/all-themes.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

</head>

<body class=" theme-red " style="background-color: #F44336;">
    <div class="login-box">
        <div class="logo">
            <img src="/images/logo.png" alt="" style="width:300px">
            
        </div>
        <div class="card">
            @yield('content')
        </div>
    </div>
    <div style="display:flex;justify-content: center;">
        <a href="https://www.maadchile.cl/desarrollo-de-aplicaciones-moviles/" style="color: white">Desarrollo de aplicaciones Maadchile {{date("Y")}}</a>
    </div>

    @yield('scripts')
</body>
</html>