<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Pagina</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>

            
        <form action="/pagePost" method="POST" enctype="multipart/form-data">
            <div class="modal-body">

                <input type="hidden" name="magazine_id" value="{{$magazine->id}}">
                    @csrf
                    <input type="hidden" id="token" value="{{csrf_token()}}">
                    <input type="hidden" name="type" value="2">
                         

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="">Portada</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">title</i>
                                </span>
                                <div class="form-line">
                                    <input type="file" name="img" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Titulo</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">title</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" required name="title" id="title" class="form-control" placeholder="Titulo">
                                </div>
                            </div>
                        </div>   
                    </div>
                    <div class="form-group row">      
                        <div class="col-md-6">
                            <label for="">Numero Pagina</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">plus_one</i>
                                </span>
                                <div class="form-line">
                                    <input type="number" required name="page" id="page" class="form-control" placeholder="Numero Pagina">
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    <div class="form-group row">         
                        <div class="col-md-12">
                            <label for="">Contenido</label>
                            <textarea name="content" id="trumbowyg-demo" class=" form-control"></textarea>
                        </div>
                    </div>           
            </div>
            <div class="modal-footer">  
                <div class=" row col-md-12 text-right">
                    <button type="submit" class="btn btn-danger" style="width:200px"> Guardar </button>
                </div>
            </div>
        </form> 
        
    </div>
</div>
</div>

