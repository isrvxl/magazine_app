@extends('layouts.app')

@section('css')
    <link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link rel="stylesheet" href="/trumbowyg/dist/ui/trumbowyg.min.css">
    <link rel="stylesheet" href="/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.min.css">
@endsection

@section('content')


    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{$error}}
            </div> 
        @endforeach

    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

  
        <div class="row clearfix">
            <div class="col-md-offset-1 col-lg-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Editar Pagina {{$page->page}} de revista {{$page->magazine->name}}
                            <div class="pull-right">
                                <form action="/pages/{{ $page->id }}" method="post" >
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-danger">Eliminar Pagina</button>
                                </form>
                            </div>
                        </h2>

                        
                        
                    </div>
            <form action="{{ route('pages.update', $page->id)}}" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                
                        @csrf
                        <input type="hidden" id="pageId" value="{{$page->id}}">
                        <input type="hidden" name="magazine_id" value="{{$page->magazine->id}}">
                        <input type="hidden" id="token" value="{{csrf_token()}}">
                        
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <label for="">Portada</label>
                                <img src="/storage/{{$page->img}}" alt="" style="max-width:300px">
                                <br>
                                <br>
                                <label for="">Editar Portada</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">title</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="file" name="img" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Titulo</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">title</i>
                                    </span>
                                    <div class="form-line">
                                    <input type="text" name="title" id="title" class="form-control" placeholder="Titulo" value="{{$page->title}}">
                                    </div>
                                </div>
                            </div>   
                            <div class="col-md-6" >
                                <label for="">Numero Pagina</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">plus_one</i>
                                    </span>
                                    <div class="form-line">
                                    <input type="number" name="page" id="page" class="form-control" placeholder="Numero Pagina" value="{{$page->page}}">
                                    </div>
                                </div>
                            </div>
                            
                                              
                            <div class="col-md-12">
                                <label for="">Descripcion</label>
                                <textarea name="content" id="trumbowyg-demo" class="form-control">{{$page->content}}</textarea>
                            </div>     
                            <div class=" row col-md-12 text-right">
                                <input type="submit" value="Actualizar" class="btn btn-danger" style="width:200px">
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="row clearfix">
        <div class="col-md-offset-1 col-lg-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Galeria</h2>
                </div>
                <div class="body">
                    <form action="/contentGallery/{{$page->id}}" id="frmFileUpload" class="dropzone" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="dz-message">
                            <div class="drag-icon-cph">
                                <i class="material-icons">touch_app</i>
                            </div>
                            <h3>Arrastra o dale Click para Subir Archivos.</h3>
                            
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                    <table class="table">
                        <thead>
                            <th>Imagen</th>
                            <th style="width: 10%;">#</th>
                        </thead>
                        <tbody>
                            @foreach ($page->galleries as $g)
                            <tr>
                                <td><img src="/images_gallery/{{$g->path}}" alt="" style="width: 200px;"></td>
                                <td>    
                                    <form id="formDelete" method="POST" action="/galleryDelete/{{$g->id}}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="sumbit" class="btn btn-danger">Borrar</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('scripts')
    <!-- Dropzone Plugin Js -->
    <script src="../../plugins/dropzone/dropzone.js"></script>

    <script src="/trumbowyg/dist/trumbowyg.min.js"></script>
    <script src="/trumbowyg/dist/plugins/lineheight/trumbowyg.lineheight.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.js"></script>
    <script src="/trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js"></script>
    <script src="/trumbowyg/dist/plugins/insertaudio/trumbowyg.insertaudio.min.js"></script>
    <script src="/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js"></script>
    <script type="/text/javascript" src="/trumbowyg/dist/langs/es.min.js"></script>
    <script src="/trumbowyg/dist/plugins/noembed/trumbowyg.noembed.min.js"></script>
        <script>
            $(function(){

                var pageId = $("#pageId").val()

                Dropzone.options.frmFileUpload = {
                    paramName: "file",
                    maxFilesize: 2
                };

                $('#trumbowyg-demo').trumbowyg({
                    lang: 'es',
                    btns: [
                        
                        ['fontfamily','formatting','strong', 'em', 'underline'],
                        ['foreColor', 'backColor'],
                        ['link'],
                        ['insertImage','upload','insertAudio', 'noembed'],
                        ['unorderedList', 'orderedList'],
                        ['viewHTML'],
                        ['undo', 'redo'], // Only supported in Blink browsers
                        ['horizontalRule'],
                        ['lineheight','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                        ['fullscreen'],
                    ],
                    plugins: {
                        upload: {
                            serverPath:"http://fuegodepentecostes.cl/contentImage"
                        }
                    }
                });
            })
        </script>
@endsection