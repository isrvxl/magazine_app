@extends('layouts.app')

@section('css')
     <!-- JQuery DataTable Css -->
     <link href="../../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
     <link rel="stylesheet" href="/trumbowyg/dist/ui/trumbowyg.min.css">
     <link rel="stylesheet" href="/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.min.css">
@endsection

@section('content')
    
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif 
        <div class="card">
            <div class="header">
                <h2>
                    Paginas de Revista {{$magazine->name}}
                </h2>
                <a href="#" data-toggle="modal" data-target="#createModal" class="btn waves-effect waves-light btn btn-info pull-right" style="margin-top: -20px">Crear Pagina</a>    
            </div>
            <div class="container row">
                @foreach ($pages as $p)                  
                <div class="col-md-2" style="margin-bottom: 2rem;">
                    <button data-id="{{$p->id}}" class="btn btn-default btn-block editPage">
                        <img src="{{asset('storage/'.$p->img)}}" alt="" style="width: 100%;">
                        <br>
                        {{$p->typelabel}} - {{$p->page}}
                    </button>
                </div>
                @endforeach
            </div>      
        </div>
    </div>
</div>
@include('mPages.create')


@endsection
@section('scripts')





<script src="/trumbowyg/dist/trumbowyg.min.js"></script>
<script src="/trumbowyg/dist/plugins/lineheight/trumbowyg.lineheight.min.js"></script>
<script src="/trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
<script src="/trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.js"></script>
<script src="/trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js"></script>
<script src="/trumbowyg/dist/plugins/insertaudio/trumbowyg.insertaudio.min.js"></script>
<script src="/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js"></script>
<script src="/trumbowyg/dist/plugins/noembed/trumbowyg.noembed.min.js"></script>
<script type="/text/javascript" src="/trumbowyg/dist/langs/es.min.js"></script>
    <script>
        $(function(){
            $('#trumbowyg-demo').trumbowyg({
                lang: 'es',
                btns: [
                    
                    ['fontfamily','formatting','strong', 'em', 'underline'],
                    ['foreColor', 'backColor'],
                    ['link'],
                    ['insertImage','upload','insertAudio', 'noembed'],
                    ['unorderedList', 'orderedList'],
                    ['viewHTML'],
                    ['undo', 'redo'], // Only supported in Blink browsers
                   
                    ['horizontalRule'],
                    ['lineheight','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                    ['fullscreen'],
                ],
                plugins: {
                    upload: {
                        serverPath:"http://fuegodepentecostes.cl/contentImage"
                    }
                }
            });

            $('#deleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id') 
    
                action = $('#formDelete').attr('data-action').slice(0,-1) 
                $('#formDelete').attr('action',action + id)
    
                var modal = $(this)
                modal.find('.modal-title').text('Vas a borrar esta pagina ' + id)
            });

            $('body').on('click', '.editPage', function(){
                var id = $(this).data('id');
                location.href=`/pages/${id}/edit`;
            })
        })
       
   </script>
@endsection