@extends('layouts.app')

@section('content')

@section('css')
    <link href="/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link rel="stylesheet" href="/trumbowyg/dist/ui/trumbowyg.min.css">
    <link rel="stylesheet" href="/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.min.css">
@endsection

@if ($errors->any())
@foreach ($errors->all() as $error)
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{$error}}
    </div> 
@endforeach

@endif

    <form action="{{ route('magazine.store')}}" method="POST" enctype="multipart/form-data">

        @csrf
        <input type="hidden" name="created_by" value="{{$user->id}}">
        <input type="hidden" id="token" value="{{csrf_token()}}">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        
        <div class="row clearfix">
            <div class=" col-md-offset-1 col-lg-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Crear Revista
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row ">
                            <div class="col-md-6">
                                <label for="">Portada</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">title</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="file" name="img" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Fondo Horizontal</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">insert_photo</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="file" name="background_image" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Titulo</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">title</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Titulo">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Estado</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">check_circle</i>
                                    </span>
                                    <select class="form-control" name="status" id="status"  placeholder="status">
                                        @foreach ($selectStatus as $key =>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" style="clear:both">
                                <label for="">Fecha de Publicacion</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="date" name="edit_date" id="edit_date" class="form-control" placeholder="Fecha Edicion">
                                    </div>
                                </div>
                            </div>    
                            <div class="col-md-12">
                                <label for="">Descripcion</label>
                                <textarea name="description" id="trumbowyg-demo" class="form-control"></textarea>
                            </div>
                            <div class=" row col-md-12 text-right">
                                <input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
                            </div>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="/trumbowyg/dist/trumbowyg.min.js"></script>
    <script src="/trumbowyg/dist/plugins/lineheight/trumbowyg.lineheight.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
    <script src="/trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
    <script src="/trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js"></script>
    <script src="/trumbowyg/dist/plugins/insertaudio/trumbowyg.insertaudio.min.js"></script>
    <script src="/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js"></script>
    <script type="/text/javascript" src="/trumbowyg/dist/langs/es.min.js"></script>
    <script src="/trumbowyg/dist/plugins/noembed/trumbowyg.noembed.min.js"></script>

        <script>
            $(function(){
                $('#trumbowyg-demo').trumbowyg({
                    lang: 'es',
                    btns: [
                        
                        ['fontfamily','formatting','strong', 'em', 'del'],
                        ['foreColor', 'backColor'],
                        ['link'],
                        ['insertImage','upload','insertAudio', 'noembed'],
                        ['unorderedList', 'orderedList'],
                        ['viewHTML'],
                        ['undo', 'redo'], // Only supported in Blink browsers
                                         ['horizontalRule'],
                        ['lineheight','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                        ['fullscreen'],
                    ],
                    plugins: {
                        upload: {
                            serverPath:"http://fuegodepentecostes.cl/contentImage"
                        }
                    }
                });
            })
        </script>
@endsection