@extends('layouts.app')

@section('css')
     <!-- JQuery DataTable Css -->
     <link href="../../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection

@section('content')

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Revistas
                    <form action="{{ route('magazine.create')}}" method="GET" class="pull-right">
                        <button type="sumbit" class="btn btn-primary">Crear Revista</button>
                    </form>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Imagen</th>
                                <th>Imagen Fondo</th>
                                <th>Descripcion</th>
                                <th>Fecha de Edicion</th>
                                <th>Creada por</th>
                                <th>Status</th>
                                <th>Acciones</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($magazines as $m)
                            <tr>
                                <td>{{$m->name}}</td>
                                <td><img src="{{asset('storage/'.$m->img)}}" width="150" /></td>
                                <td><img src="{{asset('storage/'.$m->background_image)}}" width="150" /></td>
                                <th>{!!$m->description!!}</th>
                                <th>{{$m->edit_date}}</th>
                                <th>{{$m->user->f}}</th>
                                <th>{{$m->statuslabel}}</th>
                                <th>
                                    <a href=" {{ route('magazine.edit', $m->id) }} " class="btn btn-primary"> Actualizar </a>
                                    <button data-toggle="modal" data-target="#deleteModal" data-id="{{$m->id}}" class="btn btn-danger"> Eliminar </button>
                                <a href="/magazinePage/{{$m->id}}" class="btn btn-success"> Paginas </a>
                                </th>
                            </tr>
                        @endforeach    
                        </tbody>
                    </table>
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="smallModalLabel">Seguro?</h4>
                                </div>
                                <div class="modal-body">
                                    Seguro?
                                </div>
                                <div class="modal-footer">
                                    <form id="formDelete" method="POST" action="{{ route('magazine.destroy', 0)}}" data-action="{{ route('magazine.destroy', 0)}}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="sumbit" class="btn btn-link waves-effect">Borrar</button>
                                    </form>
                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
      <!-- Jquery DataTable Plugin Js -->
      <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
      <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
  
      <!-- Custom Js -->

      <script src="/js/pages/tables/jquery-datatable.js"></script>
      
    <script>
        window.onload = function(){
            $('#deleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id') 
    
                action = $('#formDelete').attr('data-action').slice(0,-1) 
                $('#formDelete').attr('action',action + id)
    
                var modal = $(this)
                modal.find('.modal-title').text('Vas a borrar esta revista ' + id)
                });
        };
    </script>
@endsection