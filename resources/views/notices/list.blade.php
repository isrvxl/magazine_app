@extends('layouts.app')

@section('css')
     <!-- JQuery DataTable Css -->
     <link href="../../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection

@section('content')

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Avisos
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#createModal">Crear Aviso</button>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                
                                <th>Titulo</th>
                                <th>Cuerpo</th>
                                <th>Tipo</th>
                                <th>Creado Por</th>
                                <th>Fecha</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($notices as $s)
                            <tr>
                                <td>{{$s->title}}</td>
                                <td>{{$s->body}}</td>
                                <td>{{$s->type_label}}</td>
                                <td>{{$s->user->name}}</td>
                                <td>{{$s->for_at}}</td>
                                <td>
                                    <form method="POST" action="{{ route('notices.destroy', $s->id)}}" >
                                        @method('DELETE')
                                        @csrf
                                        <button type="sumbit" class="btn btn-link waves-effect">Borrar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">Crear aviso</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('notices.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Titulo</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">title</i>
                            </span>
                            <div class="form-line">
                                <input type="text" name="title" class="form-control" placeholder="Titulo de Aviso">
                            </div>
                        </div>
                    </div>
                   
                    <div  class="form-group">
                        <label for="">Cuerpo</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">textsms</i>
                            </span>
                            <div class="form-line">
                                <textarea name="body"  class="form-control" placeholder="Asunto del Mensaje"></textarea>
                            </div>
                        </div>
                    </div>
                    <div  class="form-group">
                        <label for="">Fecha</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">textsms</i>
                            </span>
                            <div class="form-line">
                                <input class="form-control"  type="date" name="for_at" >
                            </div>
                        </div>
                    </div>
                    <div  class="form-group">
                        <label for="">Tipo</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">supervisor_account</i>
                            </span>
                            <div class="form-line">
                                <select name="type" class="form-control" id="">
                                    <option value="">Seleccione</option>
                                    <option value="1">Push</option>
                                    <option value="2">Aviso</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class=" row col-md-12 text-right">
                        <input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
                    </div> 
                                
                            
                        
                    
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
      <!-- Jquery DataTable Plugin Js -->
      <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
      <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
  
      <!-- Custom Js -->

      <script src="/js/pages/tables/jquery-datatable.js"></script>
    <script>
        window.onload = function(){
            $('#deleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id') 
    
                action = $('#formDelete').attr('data-action').slice(0,-1) 
                $('#formDelete').attr('action',action + id)
    
                var modal = $(this)
                modal.find('.modal-title').text('Vas a borrar esta suscripcion: ' + id)
                });
        };
    </script>
@endsection