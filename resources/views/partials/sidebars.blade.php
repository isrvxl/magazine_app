<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="../../images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrador</div>
                <div class="email">admin@gmail.com</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>
                            {{ __('Cerrar Sesion') }}
                        </a></li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>      
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">
                    <a href="{{ route('home')}}">Inicio</a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">library_books</i>
                        <span>Revistas</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('magazine.index')}}">Ver Revistas</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">subscriptions</i>
                        <span>Suscripciones</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('subscription.index')}}">Ver Suscripciones</a>
                        </li>                
                    </ul>
                </li>
                <li>
                    <a href="/notices">
                        <i class="material-icons">textsms</i>
                        <span>Avisos</span>
                    </a>
                    
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">account_box</i>
                        <span>Usuarios</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('user.index')}}">Ver Usuarios</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/infopages/1/edit">
                        <i class="material-icons">person</i>
                        <span>Acerca de Nosotros</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; Develop by<a href="https://www.maadchile.cl" target="_blank">MaadChile 2020</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
</section>