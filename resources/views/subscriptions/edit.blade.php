@extends('layouts.app')

@section('content')

@if ($errors->any())
@foreach ($errors->all() as $error)
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{$error}}
    </div> 
@endforeach

@endif

    <form action="{{ route('subscription.update', $subscription->id)}}" method="POST">
        
        @method('PUT')
        
        @csrf
        <input type="hidden" id="token" value="{{csrf_token()}}">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        
        <div class="row clearfix">
            <div class=" col-md-offset-1 col-lg-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Actualizar Subscripcion
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Suscripcion</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">title</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" name="name" id="name" class="form-control" value="{{$subscription->name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Meses Suscripcion</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">calendar_today</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="number" name="months" class="form-control" placeholder="Meses Suscripcion" value="{{$subscription->months}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Precio CLP</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">shop_two</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="number" name="price" id="price" class="form-control" value="{{$subscription->price}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Precio Dolar</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">shop_two</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="number" name="dollar_price" class="form-control" placeholder="Dolar" value="{{$subscription->dollar_price}}">
                                    </div>
                                </div>
                            </div>
                            <div class=" row col-md-12 text-right">
                                <input type="submit" value="Actualizar" class="btn btn-danger" style="width:200px">
                            </div>   
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection