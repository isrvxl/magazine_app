@extends('layouts.auth')

@section('content')
<div class="body">
    @if (session('status')=="Suscripción Aprobada!")
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @else
        <div class="alert alert-danger">
            {{ session('status') }} <br>
            Intentelo Nuevamente mas tarde
        </div>
    @endif 
</div>
@endsection