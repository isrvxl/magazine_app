@extends('layouts.app')

@section('content')

@if ($errors->any())
@foreach ($errors->all() as $error)
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{$error}}
    </div> 
@endforeach

@endif

    <form action="{{ route('user.store')}}" method="POST">

        @csrf
        <input type="hidden" id="token" value="{{csrf_token()}}">
        <input type="hidden" name="subscription_id" value="0">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        
        <div class="row clearfix">
            <div class=" col-md-offset-1 col-lg-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Crea Usuario
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Tipo Usuario</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">supervisor_account</i>
                                    </span>
                                    <select class="form-control" name="type" id="type"  placeholder="type">
                                        <option>Tipo Usuario</option>
                                        @foreach ($selectTypes as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>        
                            <div class="col-md-6">
                                <label for="">Tipo Suscripcion</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">shop_two</i>
                                    </span>
                                    <select class="form-control" id="subscription_id"  placeholder="">
                                        <option>Tipo Suscripcion</option>
                                        @foreach ($subs as $s)
                                            <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Contraseña</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">vpn_key</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña">
                                    </div>
                                </div>
                            </div>
                            <div class=" row col-md-12 text-right">
                                <input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
                            </div>                          
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('scripts')

@endsection
