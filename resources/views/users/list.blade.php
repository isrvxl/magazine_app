@extends('layouts.app')

@section('css')
     <!-- JQuery DataTable Css -->
     <link href="../../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@endsection

@section('content')


<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Usuarios
                    <form action="{{ route('user.create')}}" method="GET" class="pull-right">
                        <button type="sumbit" class="btn btn-primary">Crear Usuario</button>
                    </form>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>                            
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th>Tipo de Suscripcion</th>
                                <th>Acciones</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $u)
                            <tr>
                                <td>{{$u->name}}</td>
                                <td>{{$u->email}}</td>
                                <th>{{$u->typelabel}}</th>
                                <th>{{$u->subscription->name ?? ''}}</th>
                                <th>
                                    <a href=" {{ route('user.edit', $u->id) }} " class="btn btn-primary"> Actualizar </a>
                                    <button data-toggle="modal" data-target="#deleteModal" data-id="{{$u->id}}" class="btn btn-danger"> Eliminar </button>
                                </th>
                            </tr>
                        @endforeach    
                        </tbody>
                    </table>
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="smallModalLabel">Seguro?</h4>
                                </div>
                                <div class="modal-body">
                                    Seguro?
                                </div>
                                <div class="modal-footer">
                                    <form id="formDelete" method="POST" action="{{ route('user.destroy', 0)}}" data-action="{{ route('user.destroy', 0)}}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="sumbit" class="btn btn-link waves-effect">Borrar</button>
                                    </form>
                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
      <!-- Jquery DataTable Plugin Js -->
      <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
      <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
      <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
  
      <!-- Custom Js -->

      <script src="/js/pages/tables/jquery-datatable.js"></script>
    <script>
        window.onload = function(){
            $('#deleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id') 
    
                action = $('#formDelete').attr('data-action').slice(0,-1) 
                $('#formDelete').attr('action',action + id)
    
                var modal = $(this)
                modal.find('.modal-title').text('Vas a borrar el usuario: ' + id)
                });
        };
    </script>
@endsection