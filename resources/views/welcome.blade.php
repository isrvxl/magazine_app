@extends('layouts.page')

@section('content')
<style>
    .carousel .carousel-item{opacity:0.9!important;top: -80px;width:300px; height: 300px;}
    .carousel{    height: 450px;}
</style>
    
<div class="body">
    <h4>Ultimas Ediciones</h4>
    
        
    <div class="row">
        @foreach ($magazines as $m)
            <div class="col s12 m6 l4" style="text-align: center;">
                <img src="{{asset('storage/'.$m->img)}}" style="width: 80%" />
                <br>
            </div>
        @endforeach
    </div>
    <br>
    <div style="display:flex;justify-content: center;">
        <a href="https://play.google.com/store/apps/details?id=com.maadchile.magazine_app&hl=es_CL">
            <img src="/images/playstore.png" alt="" style="width:200px">
        </a>
    </div>
    <div style="display:flex;justify-content: center;">
        <a href="">
            <img src="/images/appstore.png" alt="" style="width:177px">
        </a>
    </div>

    <br>
    <div style="display:flex;justify-content: center;">
        <a href="https://www.instagram.com/iep.ebenezer/">
            <img src="/images/insta.png" alt="" style="width:50px;margin-right: 10px;">
        </a>
        <a href="https://www.facebook.com/liberiaiepebenezer/">
            <img src="/images/face.png" alt="" style="width:50px;margin-right: 10px;">
        </a>
        <a href="https://www.youtube.com/channel/UCfumCKkHXQrYpB12_tf54fg/featured">
            <img src="/images/youtube.png" alt="" style="width:50px;margin-right: 10px;">
        </a>
        
    </div>
</div>
@endsection

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script>
        $(function() {
           
        })
    </script>
@endsection
