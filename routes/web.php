<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return redirect('/login');
});

Route::get('/', 'UserController@landing');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('user', 'UserController');

Route::resource('infopages', 'InfoPagesController');

Route::resource('magazine', 'MagazineController');

Route::resource('notices', 'NoticeController');

Route::post('magazine/image', 'MagazineController@image')->name('magazine.image');

Route::resource('pages', 'PageController');

Route::get('/magazinePage/{id}','MagazineController@pages');

Route::resource('subscription', 'SubscriptionController');

Route::post('/contentImage', 'MagazineController@contentImage');

Route::post('/contentGallery/{id}', 'MagazineController@contentGallery');

Route::post('/pagePost', 'PageController@pagePost');

Route::get('/goFlow', 'SaleController@goFlow');

Route::post('/flowConfirm', 'SaleController@flowConfirm');

Route::post('/flowResult', 'SaleController@flowResult');

Route::get('/appRegister', 'UserController@appRegister');

Route::get('/paypalReturn', 'SaleController@paypalReturn');

Route::get('/paypalCancel', 'SaleController@paypalCancel');

Route::post('/registerApp', 'UserController@registerApp');

Route::get('/userSuccess', 'UserController@userSuccess');

Route::get('/logout', 'UserController@logout');

Route::delete('/galleryDelete/{id}', 'PageController@galleryDelete');

Route::get('/resetPass', 'UserController@resetPass');